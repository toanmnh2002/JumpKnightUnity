using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteHander : MonoBehaviour
{
    public void MuteHandler(bool mute)
    {
        if(mute)
        {
            AudioListener.volume = 0f;
        }
        else
        {
            AudioListener.volume = 0.65f;
        }
    }
}
